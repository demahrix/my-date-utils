import 'package:flutter_test/flutter_test.dart';

import 'package:my_date_utils/src/date_utils.dart';

void main() {
  
  DateTime d1 = DateTime(2021, 12, 21, 9, 28);
  DateTime d2 = DateTime(2021, 12, 21, 9, 28, 54);
  DateTime d3 = DateTime(2021, 12, 21, 10, 34);
  DateTime d4 = DateTime(2021, 12, 21, 11, 30);
  DateTime d5 = DateTime(2021, 12, 21, 11, 48);
  DateTime d6 = DateTime(2021, 12, 21, 11, 49);
  DateTime d7 = DateTime(2021, 12, 21, 19, 00);
  DateTime d8 = DateTime(2021, 12, 22, 17, 23);
  DateTime d9 = DateTime(2021, 12, 24, 17, 23);

  group("Format date", () {

    test('description', () {

      expect(MyDateUtils.format(d1), '21/12/2021');
      expect(MyDateUtils.format(
        d1,
        includeTime: true,
        separator: "-"
      ), '21-12-2021 a 09:28');

      expect(MyDateUtils.format(d1, type: DateUtilsFormat.long), 'Mardi, 21 décembre 2021');

    });

  });

  group('realtive date', () {

    test('', () {

      expect(MyDateUtils.relativeString(d1, d2), 'Il y a un instant');
      expect(MyDateUtils.relativeString(d1, d3), 'Il y a une heure');
      expect(MyDateUtils.relativeString(d2, d3), 'Il y a une heure');
      expect(MyDateUtils.relativeString(d1, d4), 'Il y a 2 heures');
      expect(MyDateUtils.relativeString(d5, d4), 'Il y a 18 minutes');
      expect(MyDateUtils.relativeString(d5, d6), 'Il y a une minute');
      expect(MyDateUtils.relativeString(d1, d7), 'Aujourd\'hui a 09:28');
      expect(MyDateUtils.relativeString(d1, d8), 'Hier a 09:28');
      expect(MyDateUtils.relativeString(d1, d9), '21/12/2021 a 09:28');

    });

  });

}
