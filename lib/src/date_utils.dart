import 'package:flutter/material.dart';

enum DateUtilsFormat {

  /// exemple: 12/12/2012
  short,

  /// exemple: Samedi, 12 juin 2021
  long,

  /// `f1` ex: Mer, 12 Déc
  f1,

  /// `f2` ex: 12 Déc. 2012 / 12 Déc, 2012
  f2,

  /// `f2_2` ex: 12 Déc.
  f2_2
}

enum DaySizePreference {
  short,
  medium,
  long
}

enum MonthSizePreference {
  short,
  medium,
  long
}

class MyDateUtils {

  static final List<String> _shortDays = ['L', 'M', 'M', 'J', 'V', 'S', 'D'];
  static final List<String> _mediumDays = ['Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam', 'Dim'];
  static final List<String> _days = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'];

  static final List<String> _shortMonths = ['J', 'F', 'M', 'A', 'M', 'J', 'J', 'A', 'S', 'O', 'N', 'D'];
  static final List<String> _mediumMonths = ['Jan', 'Fév', 'Mars', 'Avr', 'Mai', 'Juin', 'Juil', 'Août', 'Sep', 'Oct', 'Nov', 'Déc'];
  static final List<String> _months = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];


  static minDate(DateTime date) {
    return DateTime(date.year, date.month, date.day);
  }

  static maxDate(DateTime date) {
    return DateTime(date.year, date.month, date.day, 23, 59, 59, 999, 999);
  }

  /// Permet de format une date, `type` défini le type \
  /// `short` ex: 12/12/2012\
  /// `long` ex: Mercredi, 12 décembre 2012
  /// `f1` ex: Mer, 12 Déc
  /// `f2` ex: 12 Déc, 2012, 12 Déc. 2012 
  /// FIXME add medium ex: 12 décembre 2012
  /// FIXME add medium2 ex: 12 déc. 2012
  static String format(
      DateTime d, {
        DateUtilsFormat type = DateUtilsFormat.short,
        String separator = '/',
        bool includeTime = false
      }
  ) {

    String? time;

    if (includeTime)
      time = ' ${_add0(d.hour)}:${_add0(d.minute)}'; // ' à ${_add0(d.hour)}:${_add0(d.minute)}'; // typo

    late String date;

    switch(type) {
      case DateUtilsFormat.short:
        date = _formatShort(d, separator);
      break;
      case DateUtilsFormat.long:
        date = _formatLong(d);
      break;
      case DateUtilsFormat.f1:
        date = _formatF1(d);
      break;
      case DateUtilsFormat.f2:
        date = _formatF2(d, separator: separator);
        break;
      case DateUtilsFormat.f2_2:
        date = _formatF2_2(d, separator: separator);
      break;
    }

    return includeTime ? date + time! : date;
  }

  static String formatTime(DateTime d) {
    return '${_add0(d.hour)}:${_add0(d.minute)}';
  }

  static String _formatShort(DateTime d, String sep) {
    return '${_add0(d.day)}$sep${_add0(d.month)}$sep${d.year}';
  }

  static String _formatLong(DateTime d) {
    return '${_days[d.weekday - 1]}, ${_add0(d.day)} ${_months[d.month - 1]} ${d.year}';
  }

  static String _formatF1(DateTime d) {
    var i = d.month;
    var day = _days[d.weekday - 1].substring(0, 3);
    var mouth = _months[d.month - 1].substring(0, i == 6 || i == 7 || i == 8 ? 4 : 3);
    return '$day, ${_add0(d.day)} $mouth';
  }

  static String _formatF2(DateTime d, { String separator = '.' }) {
    var mouth = _mediumMonths[d.month - 1];
    return '${_add0(d.day)} $mouth${separator} ${d.year}';
  }

  static String _formatF2_2(DateTime d, { String separator = '.' }) {
    var mouth = _mediumMonths[d.month - 1];
    return '${_add0(d.day)} $mouth${separator}';
  }

  static String relativeString(DateTime d1, [ DateTime? d2 ]) {
    d2 ??= DateTime.now();

    int t1 = d1.millisecondsSinceEpoch;
    int t2 = d2.millisecondsSinceEpoch;

    int diff; // en secondes
    DateTime old;

    if (t1 > t2) {
      diff = (t1 - t2) ~/ 1000;
      old = d2;
    } else {
      diff = (t2 - t1) ~/ 1000;
      old = d1;
    }

    if (diff < 60)
      return 'Il y a un instant';
    
    int minutes = diff ~/ 60;

    if (diff < 3600)
      return "Il y a ${minutes < 2 ? 'une' : minutes} minute${minutes < 2 ? '' : 's'}";

    int hours = diff ~/ 3600;

    if (hours < 4)
      return "Il y a ${hours < 2 ? 'une' : hours} heure${hours < 2 ? '' : 's'}";

    String timeStr = 'à ${_add0(old.hour)}:${_add0(old.minute)}';

    // hier ou aujourd'hui
    if (hours < 48)
      return "${d1.day == d2.day ? 'Aujourd\'hui' : 'Hier'} $timeStr"; // typo

    return '${_formatShort(old, '/')} $timeStr';
  }

  static String _add0(int n) => n < 10 ? '0$n' : n.toString();




  /// test
  static DateTime startOfDay(DateTime date) {
    return DateTime(date.year, date.month, date.day);
  }

  /// test
  static DateTime endOfDay(DateTime date) {
    return DateTime(date.year, date.month, date.day, 23, 59, 59, 999, 999);
  }

  /// test
  static DateTime startOfWeek(DateTime date) {
    // DateTime(date.year, date.month, date.day - date.weekday % 7) // Le Dimanche le plus recent
    return DateTime(date.year, date.month, date.day - (date.weekday - 1)); // Le lundi le plus recent
  }

  /// test
  static DateTime endOfWeek(DateTime date) {
    return DateTime(date.year, date.month, date.day, 23, 59, 59, 999, 999);
  }

  /// test
  static DateTime startOfMonth(DateTime date) {
    return DateTime(date.year, date.month, 1);
  }

  /// test
  static DateTime endOfMonth(DateTime date) {
    return DateTime(date.year, date.month + 1, 0);
  }

  /// test
  static DateTime startOfYear(DateTime date) {
    return DateTime(date.year, 1, 1);
  }

  /// test
  static DateTime endOfYear(DateTime date) {
    return DateTime(date.year, 12, 31);
  }

  static DateTimeRange getWeekRangeFrom(int year, int weekNumber) {
    return getDatesFromWeekOfYearNumber(year, weekNumber);
  }

  // test
  static DateTimeRange getDatesFromWeekOfYearNumber(int year, int weekNumber) {
    // first day of the year
    final DateTime firstDayOfYear = DateTime.utc(year, 1, 1);

    // first day of the year weekday (Monday, Tuesday, etc...)
    final int firstDayOfWeek = firstDayOfYear.weekday;

    // Calculate the number of days to the first day of the week (an offset)
    final int daysToFirstWeek = (8 - firstDayOfWeek) % 7;

    // Get the date of the first day of the week
    final DateTime firstDayOfGivenWeek = firstDayOfYear.add(Duration(days: daysToFirstWeek + (weekNumber - 1) * 7));

    // Get the last date of the week
    final DateTime lastDayOfGivenWeek = firstDayOfGivenWeek.add(Duration(days: 6));

    // Return a WeekDates object containing the first and last days of the week
    return DateTimeRange(start: firstDayOfGivenWeek, end: lastDayOfGivenWeek);
  }

  /// test
  static int getWeekOfYearNumber(DateTime date) {
    int daysToAdd = DateTime.thursday - date.weekday;
    DateTime thursdayDate = daysToAdd > 0 ? date.add(Duration(days: daysToAdd)) : date.subtract(Duration(days: daysToAdd.abs()));
    int dayOfYearThursday = getDayOfYearNumber(thursdayDate);
    return 1 + ((dayOfYearThursday - 1) / 7).floor();
  }

  /// test
  static int getDayOfYearNumber(DateTime date) {
    return date.difference(DateTime(date.year, 1, 1)).inDays;
  }





  /// [index]: [0, 6] [Lundi, Dimanche]
  static String dayOf(int index, [DaySizePreference size = DaySizePreference.long]) {
    switch(size) {
      case DaySizePreference.long:
        return _days[index];
      case DaySizePreference.medium:
        return _mediumDays[index];
      case DaySizePreference.short:
        return _shortDays[index];
    }
  }

  /// [index]: [1, 12]
  static String monthOf(int index, [MonthSizePreference size = MonthSizePreference.long]) {
    switch(size) {
      case MonthSizePreference.long:
        return _months[index - 1];
      case MonthSizePreference.medium:
        return _mediumMonths[index - 1];
      case MonthSizePreference.short:
        return _shortMonths[index - 1];
    }
  }

  /// test
  /// 09-15 Nov OU 15 Nov-07 Dec OU 15 Nov 24-07 Dec 25
  static String formatDateRange(DateTime startDate, DateTime endDate) {
    bool isSameYear = startDate.year == endDate.year;
    bool isSameMonth = startDate.month == endDate.month;

    if (isSameYear && isSameMonth && startDate.day == endDate.year)
      return '${_add0(startDate.day)} ${_mediumMonths[startDate.month - 1]}';
    if (isSameYear && isSameMonth)
      return '${_add0(startDate.day)}-${_add0(endDate.day)} ${_mediumMonths[endDate.month - 1]}';
    if (isSameYear)
      return '${_add0(startDate.day)} ${_mediumMonths[startDate.month - 1]}-${_add0(endDate.day)} ${_mediumMonths[endDate.month - 1]}';
    return '${_add0(startDate.day)} ${_mediumMonths[startDate.month - 1]} ${startDate.year.toString().substring(2)}-${_add0(endDate.day)} ${_mediumMonths[endDate.month - 1]} ${endDate.year.toString().substring(2)}';
  }

}
