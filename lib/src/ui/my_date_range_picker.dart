import 'package:flutter/material.dart';

/// FIXME SET ICON
class MyDateRangePicker extends StatefulWidget {

  final DateTimeRange? initialRange;
  final Widget? icon;
  final bool showIcon;
  final ValueChanged<DateTimeRange> onChanged;
  final DateTime? firstDate;
  final DateTime? lastDate;

  final bool useRootNavigator;
  final DatePickerEntryMode initialEntryMode;

  const MyDateRangePicker({
    super.key,
    this.initialRange,
    this.icon,
    this.showIcon = true,
    required this.onChanged,
    this.firstDate,
    this.lastDate,
    this.useRootNavigator = true,
    this.initialEntryMode = DatePickerEntryMode.calendar
  });

  @override
  State<MyDateRangePicker> createState() => _MyDateRangePickerState();
}

class _MyDateRangePickerState extends State<MyDateRangePicker> {

  static final FIRST_DATE = DateTime(1970);
  static final LAST_DATE = DateTime(2099, 12, 31, 23, 59, 59, 999, 999);

  DateTimeRange? _range;

  @override
  void initState() {
    super.initState();
    _range = widget.initialRange;
  }

  void _showDialog() async {
    final range = await showDateRangePicker(
      context: context,
      initialDateRange: _range,
      useRootNavigator: widget.useRootNavigator,
      initialEntryMode: widget.initialEntryMode,
      firstDate: widget.firstDate ?? FIRST_DATE,
      lastDate: widget.lastDate ?? LAST_DATE
    );

    if (range != null) {
      setState(() { _range = range; });
      widget.onChanged(range);
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: _showDialog,
      child: MouseRegion(
        cursor: SystemMouseCursors.click,
        child: Container(
          // width: 305.0,// 260.0,
          height: 42.0,
          padding: const EdgeInsets.symmetric(horizontal: 12.0),
          decoration: BoxDecoration(
            color: Colors.grey[200],
            borderRadius: const BorderRadius.all(Radius.circular(6.0))
          ),
          child: Row(
            children: [
              if (widget.showIcon)
                Padding(
                  padding: const EdgeInsets.only(right: 8.0),
                  child: widget.icon ?? const Icon(Icons.calendar_month_outlined, color: Colors.orange, size: 20.0),
                ),
              const Icon(Icons.keyboard_arrow_left_rounded),
              _getDateOrPlaceholder(),
              const Icon(Icons.keyboard_arrow_right_rounded)
            ],
          ),
        ),
      ),
    );
  }

  Widget _getDateOrPlaceholder() {
    bool isSelected = _range != null;
    final MaterialLocalizations localizations = MaterialLocalizations.of(context);

    return Text(
      isSelected
        ? '${localizations.formatShortDate(_range!.start)} - ${localizations.formatShortDate(_range!.end)}'
        : '--/--/---- - --/--/----',
      style: TextStyle(
        fontWeight: FontWeight.w600,
        color: isSelected ? null : Colors.grey
      ),
    );
  }

}
